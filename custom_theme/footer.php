



    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <h3 class="text-secondary">Get Started</h3>
                  <a href="#" style="text-decoration:none;"> <h6 class="text-warning pt-5 pb-5 ">Register</h6></a> 
                    <h3 class="text-secondary pb-3">Learn more</h3>
                    <h6 class="text-warning">About CapSource</h6>
                    <h6 class="text-warning">Experiential Learning</h6>
                    <h6 class="text-warning">Blog</h6>
                    <h6 class="text-warning">videos</h6>
                    <h6 class="text-warning">Pricing</h6>
                    <h6 class="text-warning">Knowledge Base</h6>
                    <h3 class="text-secondary pt-5 pb-4">Explore</h3>
                    <h6 class="text-warning">Company Directory</h6>
                    <h6 class="text-warning">Engagement Formats & Samples
                        <h6 class="text-warning"> Project Departments & Templates </h6>
                        <!-- <img src="./footer_img.png" style="height: 80px; width: 80px;" alt=""> -->
                      
                        <img  style="width:140px;height:120px; padding-top:20px;" src="<?php echo get_bloginfo( 'template_directory' ); ?>./footer_img.png" />
                      
                </div>
                <div class="col-sm">
                    <h3 class="text-secondary pb-3">Our Services</h3>
                    <h6 class="text-warning">Connect – White-Label Software</h6>
                    <h6 class="text-warning">Company Sourcing</h6>
                    <h6 class="text-warning">Scoping & Instructional Design</h6>
                    <h6 class="text-warning">Program Marketing Videos</h6>
                    <h4 class="text-secondary pb-3">Legal</h4>
                    <h6 class="text-warning">Terms of Service</h6>
                    <h6 class="text-warning">Privacy Policy</h6>
                    <h3 class="text-secondary pt-5 pb-4">Contact Us</h3>
                    <span class="d-block text-warning">
                        <i class="fa fa-link" aria-hidden="true"></i>
                        FAQ & Contact
                    </span>
                    <span class="d-block text-warning">
                        <i class="fa fa-link" aria-hidden="true"></i>
                        support@capsource.io
                    </span>
                    <span class="d-block text-warning">
                        <i class="fa fa-link" aria-hidden="true"></i>
                        (631) 729-0745
                    </span>
                    <span class="d-block text-secondary">
                        <i class="fa fa-link" aria-hidden="true"></i>
                        StartEd – EdTech Incubator, 900 Third Ave, 29th Floor, New York, NY 10022
                    </span>

                    <span>
                        <a href="#">
                            <i class="fa fa-facebook-square text-white hover-1" style="font-size:36px"></i>
                        </a>

                        <a href="#">
                            <i class="fa fa-linkedin-square text-white hover-1" style="font-size:36px"></i>
                            
                        </a>
                        <a href="#">
                            <i class="fa fa-twitter-square text-white hover-1" style="font-size:36px"></i>
                        </a>

                    </span>
                </div>
                <div class="col-sm ">
                    <form style="background-color: #CC3131; margin-top:-100px; z-index: 99; border-top-left-radius: 15px;border-top-right-radius: 15px">
                        <div class="form-group">
                            <h5 class="text-center text-white p-5">Discover experiential learning opportunities in our monthly newsletter!</h5>
                            <label for="exampleInputEmail1" class="text-white pl-4 pr-2">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-white pl-2 pr-2">We'll never share your email with anyone else.</small>
                            <label for="exampleInputEmail1" class="text-white pl-2 pr-2>First Name</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            <label for="exampleInputEmail1" class="text-white pl-3">Last Name</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="text-white pl-4 pr-2">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input ml-1" id="exampleCheck1">
                            <label class="form-check-label text-white ml-4" for="exampleCheck1">Company Contact</label> <br>
                            <input type="checkbox" class="form-check-input ml-1" id="exampleCheck1">
                            <label class="form-check-label text-white ml-4" for="exampleCheck1">Professor</label> <br>
                            <input type="checkbox" class="form-check-input ml-1" id="exampleCheck1">
                            <label class="form-check-label text-white ml-4" for="exampleCheck1">HS Educator</label> <br>
                            <input type="checkbox" class="form-check-input ml-1" id="exampleCheck1">
                            <label class="form-check-label text-white ml-4" for="exampleCheck1">Student</label>
                       
                        </div>
                        <button  type="submit" class="btn btn-light text-danger ml-2 mb-5">Submit</button>
                    </form>
                </div>

              
            </div>
            <div class="row">
                <div class="col-sm text-center">
                    <span class="text-secondary">© 2017-2020 CapSource Education LLC</span>
                </div>
            </div>

          
        </div>


    </section>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->

    <?php wp_footer(); ?>
</body>

</html>