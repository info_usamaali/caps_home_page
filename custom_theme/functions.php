
<?php

function my_scripts() {
    wp_enqueue_style('bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css');
    wp_enqueue_script( 'boot1','https://code.jquery.com/jquery-3.3.1.slim.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot2','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot3','https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ),'',true );
    wp_enqueue_style('style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css');
    // wp_enqueue_style ('theme-style', get_template_directory_uri().'/css/style.css');
    // wp_enqueue_style ('my-style', get_template_directory_uri().'./style.css', array('theme-style'));
    wp_enqueue_style( 'custom-style', get_template_directory_uri() . './style.css' );

    wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . './owl.carousel.min.js', array( 'jquery' ), '', true ); 
    wp_enqueue_style( 'owl-style-min', get_stylesheet_directory_uri() . './owl.carousel.min.css' ); 
    wp_enqueue_style( 'owl-style-def', get_stylesheet_directory_uri() . './owl.theme.default.min.css' );
    wp_enqueue_script( 'slider-js', get_template_directory_uri() . './slider.js', array( 'jquery' ), '', true );  
    // add_action( 'wp_enqueue_scripts', 'loadup_scripts' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts' );


// function custom_scripts() {
//     wp_enqueue_style( 'bootstrap-style' , 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' );
//     wp_enqueue_script( 'custom-script', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', array( 'jquery' ), false, true );
//     wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/style.css' );
//     }
//     add_action( 'wp_enqueue_scripts', 'custom_scripts' );

