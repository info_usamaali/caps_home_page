<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
        crossorigin="anonymous"> -->
  


    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="./owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="./slider.js"></script>
    <link rel="stylesheet" href="style.css"> -->


    <title>Hello, world!</title>
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="d-flex flex-nowrap w-10">
                    <a class="navbar-brand" href="">
                        <img class="img-fluid logo pl-5" src="https://capsource.io/wp-content/themes/capstonesource/images/CapSource_logo.png " style="width: 40%;height: 100%;"
                            alt="">
                    </a>
                    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Educators
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Companies
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Student</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Learn more
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link" href="#">Register</a>
      </li> -->
    </ul>
  </div>
</nav>
            <!-- <nav class="navbar navbar-expand-md navbar-light fixed-top bg-white">
                <div class="d-flex flex-nowrap w-100">
                    <a class="navbar-brand" href="">
                        <img class="img-fluid logo pl-5" src="https://capsource.io/wp-content/themes/capstonesource/images/CapSource_logo.png " style="width: 40%;height: 100%;"
                            alt="">
                    </a>
                    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                       <li class="nav-item active">
                            <a class="nav-link" href="index.html">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li> -->

                        <!-- <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Educators</a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">Inbox</a>
                                <a href="#" class="dropdown-item">Drafts</a>
                                <a href="#" class="dropdown-item">Sent Items</a>
                                <div class="dropdown-divider"></div>
                                <a href="#" class="dropdown-item">Trash</a>

                            </div>

                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Companies</a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">Inbox</a>
                                <a href="#" class="dropdown-item">Drafts</a>
                                <a href="#" class="dropdown-item">Sent Items</a>
                                <div class="dropdown-divider"></div>
                                <a href="#" class="dropdown-item">Trash</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="">Student</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Learn More</a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">Inbox</a>
                                <a href="#" class="dropdown-item">Drafts</a>
                                <a href="#" class="dropdown-item">Sent Items</a>
                                <div class="dropdown-divider"></div>
                                <a href="#" class="dropdown-item">Trash</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="about.html">Register</a>
                        </li>

                        <li class="nav-item mr-5">
                            <a class="nav-link" href="about.html">Login</a>
                        </li>
                    </ul>
                </div>
            </nav> --> 
        </div>
    </header>

    <!-- Section Header-->
    <section class="main-header">
        <div class="col-sm-12">
            <!-- <img src="./vip.png" style="padding-left: 200px; padding-top: 105px;" alt=""> -->
            <img style="padding-left:280px; padding-top:130px;" src="<?php echo get_bloginfo( 'template_directory' ); ?>./vip.png" />
        </div>
        <h4 class="text-center text-white pt-5">CUSTOMIZE THE EXh4ERIENCE TO YOUR BUSINESS NEEDS.</h4>
        <h4 class="text-center text-white pt-4">Choose from these formats.</h4>
    </section>
    <h1 class="text-center pt-5">Featured Industry Partners</h1>
    <div class="carousel-wrap">
        <div class="owl-carousel">
            <!-- <div class="item">
                <img src="./img1.jpg">
                
            </div> -->
            
            <img style="width:240px; height:200px;" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img1.jpg" />
            <img style="width:240px; height:200px;" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img1.jpg" />
            <img style="width:240px; height:200px;" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img1.jpg" />
            <img style="width:240px; height:200px;"  src="<?php echo get_bloginfo( 'template_directory' ); ?>/img1.jpg" />
            <img style="width:240px; height:200px;" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img1.jpg" />
            <img style="width:240px; height:200px; " src="<?php echo get_bloginfo( 'template_directory' ); ?>/img1.jpg" />
          

            
        </div>
    </div>

